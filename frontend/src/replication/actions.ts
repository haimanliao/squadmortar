import {SessionActionType, SessionAction, Session, User } from './types';
import Peer, { DataConnection } from 'peerjs';
import * as RB from './peerjsPrimitives'; // "Reliable Broadcast"



export const createSession: () => SessionAction = 
  () => ({type: SessionActionType.create, payload: {}});

export const sessionStarted: (id: string) => SessionAction = 
  (id) => ({type: SessionActionType.started, payload: {id}});

export const addUser: (user: User) => SessionAction = 
  (user) => ({type: SessionActionType.addUser, payload: {user}})

export const join: (id: Session["id"]) => SessionAction = 
  (id) => ({type: SessionActionType.join, payload: {id}});

export const sendMessage: (userId: any, message: any) => SessionAction =
  (userId, message) => ({type: SessionActionType.sendMessage, payload: {userId, message}})
/*
const endSession: () => ReplicationAction = 
  // cleanup currently done in reducer...
  () => ({type: ReplicationActionType.endSession, payload: {}});


////---- 
export const sendMessage: (connectionLabel: any, message: any) => (dispatch: any, getState: any) => void =
  (connectionLabel, message) => (dispatch, getState) => {
    const replication: ReplicationState = getState().replication;
    const session = replication.get("session")!
    RB.send(session, connectionLabel, message);
  }

export const broadcastMessage: (message: any) => (dispatch: any, getState: any) => void =
  (message) => (dispatch, getState) => {
    const replication: ReplicationState = getState().replication;
    const session = replication.get("session")!
    RB.broadcast(session, message);
  }
*/