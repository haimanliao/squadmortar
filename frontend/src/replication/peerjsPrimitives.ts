import { Session, ReplicationState } from "./types"
import { DataConnection } from 'peerjs';
import Peer from 'peerjs';


////---- setup
export const newPeer: (
  onSessionReady: (peer: Peer) => any, 
  onConnectionReady: (conn: DataConnection) => any,
  onMessageReceived: (message: any) => any,
) => Peer = 
  (onSessionReady, onConnectionReady, onMessageReceived) => {
    let peer = new Peer({
      host: '192.168.88.252',
      port: 9000,
      path: '/myapp'
    });
    peer.on('connection', configureConnection(onConnectionReady, onMessageReceived));
    peer.on('open', (id: string) => {
      console.log('My peer ID is: ' + id);
      onSessionReady(peer)
    });
    return peer;
  }

const configureConnection: (
    onReady: (conn: DataConnection) => any,
    onMessageReceived: (message: any) => any,
  ) => 
  (conn: DataConnection) => 
  DataConnection 
  = 
  (onReady, onMessageReceived) => 
  (conn) => 
  {
    console.log('Connection incoming: ', conn);
    conn.on('open', () => {
      console.log('Connection open: ', conn);
      conn.on('data', (data: any) => {
        console.log("received: ", data);
        onMessageReceived(data);
      });
      conn.send(`Connection ready: ${conn}`);
      onReady(conn);
    });
    return conn;
  }

// export const destroyPeer = () => {}

export const connect = (
    state: ReplicationState, 
    peerId: string, 
    onReady: (conn: DataConnection) => any,
    onMessageReceived: (message: any) => any,
  ) => {
  let conn = state.get("peer").connect(peerId);
  return configureConnection(onReady, onMessageReceived)(conn);
}

export const send: (state: ReplicationState, connectionId: string, message: any) => void =
  (state, connectionId, message) => {
    console.log("trying to send to", connectionId,  "with state", state.toJS())
    const conn = state?.get("connections").get(connectionId);
    if (conn){
      console.log("Sending message ", message, "to", connectionId);
      conn.send(message);
    } else{
      console.log("no connection");
    }
  }

export const broadcast: (state: ReplicationState,  message: any) => void =
  (state, message) => {
    const connections = state.get("connections").valueSeq();
    connections.forEach((conn) => {
      console.log("Sending broadcast message ", message, "to", conn.peer);
      conn.send(message);
    })
  }
/*


////---- 
export const send: (session: Session, connectionId: any, message: any) => void =
  (session, connectionId, message) => {
    const conn = session.get("connections").get(connectionId);
    if (conn){
      console.log("Sending message ", message, "to", connectionId);
      conn.send(message);
    } else{
      console.log("no connection");
    }
  }

export const broadcast: (session: Session,  message: any) => void =
  (session, message) => {
    const connections = session.get("connections").valueSeq();
    connections.forEach((conn) => {
      console.log("Sending broadcast message ", message, "to", conn.peer);
      conn.send(message);
    })
  }

  */