import Peer, { DataConnection } from "peerjs";
import { Map, List, RecordOf, Record } from 'immutable';


export const newSession = Record({
  id: "",
  users: Map<string, User>()
})
export type Session =  RecordOf<{
  id: string;
  users: Map<string, User>;
}>;

export const newReplicationState = Record({
  peer: new Peer(),
  connections: Map<string, DataConnection>()
  
})
export type ReplicationState = RecordOf<{
  peer: Peer;
  connections: Map<string, DataConnection>;
}>;

export enum ReplicationActionType {
  createSession = "REPLICATION_CREATE_SESSION",
  endSession = "REPLICATION_END_SESSION",
  connectTo = "REPLICATION_CONNECT_TO",
  connectionReady = "REPLICATION_CONNECTION_READY",
  sendMessage = "REPLICATION_SEND_MESSAGE",
  receiveMessage = "REPLICATION_RECEIVE_MESSAGE",
  broadcastMessage = "REPLICATION_BROADCAST_MESSAGE",
};




export type ReplicationAction 
  = {type: ReplicationActionType.createSession, payload: {peer: Peer}}
  | {type: ReplicationActionType.endSession, payload: {}}
  | {type: ReplicationActionType.connectTo, payload: {connection: DataConnection}}
  | {type: ReplicationActionType.connectionReady, payload: {connection: DataConnection}}
  | {type: ReplicationActionType.receiveMessage, payload: {message: Message}}


export enum SessionActionType {
  create = "SESSION_CREATE",
  started = "SESSION_STARTED",
  end = "SESSION_END",
  join = "SESSION_JOIN",
  leave = "SESSION_LEAVE",
  addUser = "SESSION_ADD_USER",
  removeUser = "SESSION_REMOVE_USER",
  sendMessage = "REPLICATION_SEND_MESSAGE",
};




export type SessionAction 
  = {type: SessionActionType.create, payload: {}}
  | {type: SessionActionType.started, payload: {id: Session["id"]}}
  | {type: SessionActionType.end, payload: {}}
  | {type: SessionActionType.join, payload: {id: Session["id"]}}
  | {type: SessionActionType.leave, payload: {}}
  | {type: SessionActionType.addUser, payload: {user: User}}
  | {type: SessionActionType.removeUser, payload: {userId: User["id"]}}
  | {type: SessionActionType.sendMessage, payload: {message: any, userId: string}}


export type User = RecordOf<{
  id: string,
}>
export const newUser = Record({
  id: "",
})

export type Message = any
export enum MessageType {
  action = "MESSAGE_ACTION",
  heartbeat = "MESSAGE_HEARTBEAT",
  ping = "MESSAGE_PING"
}
/*
class Session{
  constructor(){
    this.peer = new Peer();
    this.peer.on('open', function(id) {
      console.log('My peer ID is: ' + id);
    });
    this.peer.on('connection', (conn) => {
      conn.on('open', () => {
        conn.send('Connection ready.');
      });
      conn.on('data', (data) => {
        console.log("received: ", data);
      });
    });
  }
  connect(id){
    this.conn = this.peer.connect(id);
    return this.conn;
  }
  broadcast(msg){

  }
}

var session = new Session();
*/