import { newSession, newReplicationState, ReplicationActionType as RAT, ReplicationState, Session, SessionActionType as SAT, ReplicationAction, SessionAction, User } from './types';
import { List, Map } from 'immutable';
import { DataConnection } from 'peerjs';

// "replication" is an implementation detail here
export const replicationReducer = (state: ReplicationState, action: ReplicationAction) => {
  if (state === undefined){
    return null;
  }
  switch(action.type){
    case RAT.createSession:
      return newReplicationState({peer: action.payload.peer})
    case RAT.connectionReady:
      return addConnection(state, action.payload.connection)
    default:
      return state;
  }
}

export const addConnection = (state:ReplicationState, connection: DataConnection) => 
  state.update("connections", (connections) => connections.set(connection.peer, connection));

// "session" is a feature, i.e. visible to UI/user
export const sessionReducer = (state: Session, action: SessionAction) => {
  if (state === undefined){
    return null;
  }
  switch(action.type){
    case SAT.started:
      return newSession({id: action.payload.id})
    case SAT.end:
      return null
    case SAT.addUser:
      return state ? addUser(state, action.payload.user) : state;
    case SAT.removeUser:
      return state ? removeUser(state, action.payload.userId) : state;
    default:
      return state;
  }
}

const cleanupSession = (s: ReplicationState) => {
  if (s.get("peer")){
    s.get("peer").destroy();
  }
}

const addUser = (state: Session, user: User) => {
  return state.update("users", (cm: Map<string, User>) => cm.set(user.id, user));
}
const removeUser = (state: Session, userId: User["id"]) => {
  return state.update("users", (cm: Map<string, User>) => cm.remove(userId));
}