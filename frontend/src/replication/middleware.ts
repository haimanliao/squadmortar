import { ReplicationState, ReplicationActionType as RAT, newReplicationState, SessionActionType as SAT, newUser, Message, User, MessageType} from "./types"
import Peer = require("peerjs");
import * as RB from "./peerjsPrimitives";
import { sessionStarted, addUser } from "./actions";
import { DataConnection } from "peerjs";
import { WeaponActionType } from "../world/weapon/types";
import { EntityActionType } from "../world/types";
import { MinimapActionType } from "../minimap/types";
import { UserSettingsActionType } from "../ui/types";

// listen on SAT, do stuff, pass on SAT
export const replicationMiddleware = (store: any) => (next:any) => (action: any) => {
  const replacementAction = handler(store, action);
  return next(replacementAction || action);
}

// preventing circles is a dev responsibility.
// in general: return or dispatch, but not both
const handler = (store: {getState: any, dispatch: any}, action: any) => {
  const replicationState: ReplicationState = store.getState().replication;
  const dispatch = store.dispatch
  switch(action.type){
    case SAT.create:
      create(dispatch);
      return null
    case SAT.join:
      console.log("joining", action.payload)
      join(dispatch, replicationState, action.payload.id)
      return null
    case SAT.sendMessage:
      console.log("sending", action.payload)
      send(replicationState, action.payload.userId, action.payload.message)
      return null
    case RAT.receiveMessage:
      console.log("handling", action.payload)
      return handleMessage(dispatch, action.payload.message)

    case EntityActionType.add:
    case MinimapActionType.set:
    case UserSettingsActionType.write:
      if (replicationState){
        broadcast(replicationState, {type: MessageType.action, action})
      }
      return null
    default:
      return null;
  }

}

const handleMessage = (dispatch: Function, message: Message) => {
  if (message.type === MessageType.action){
    console.log("dispatching message as action")
    return message.action
  }
  return null
}

const onConnectionReady = (dispatch: Function) => (connection: DataConnection) => {
  console.log("on conn ready called", connection)
  dispatch({type: RAT.connectionReady, payload: {connection}})
  dispatch(addUser(newUser({id: connection.peer})))
}

const onMessageReceived = (dispatch: Function) => (message: Message) => {
  dispatch({type: RAT.receiveMessage, payload: {message}})
}
const create = (dispatch: Function) => {
  const onSessionReady = (peer: Peer) => dispatch(sessionStarted(peer.id))
  const peer = RB.newPeer(onSessionReady, onConnectionReady(dispatch), onMessageReceived(dispatch))
  dispatch({type: RAT.createSession, payload: {peer}}) 
}

const join = (dispatch: Function, state: ReplicationState, id: string) => {
  const connection = RB.connect(state, id, onConnectionReady(dispatch), onMessageReceived(dispatch));
}

const send = (state: ReplicationState, userId: User["id"], message: any) => {
  return RB.send(state, userId, message)
}
const broadcast = (state: ReplicationState, message: any) => {
  return RB.broadcast(state, message)
}