import log from "./render/types";
import { newStore, dispatch } from './store';
import { setupEventsAndInit } from './events';
import * as M from "gl-matrix"
import { changeMap, newWriteAction, settingsToActions } from "./ui/actions";
import { drawTexture, drawAll } from './render/canvas';
import * as React from "react";
import * as ReactDOM from 'react-dom'
import { Provider, connect  } from 'react-redux'
import { userSettings } from './ui/reducer';
import { makeLeftPanel, mapOptions } from './ui/leftPanel';
import {Map} from "immutable"
import { makeRightPanel } from './ui/rightPanel';
import { makeTooltip } from "./ui/tooltip";
import { addWeapon } from "./world/weapon/actions";
import { S5Map } from "./s5/s5map";
import { $s5image, $s5canvas } from "./elements";
import { loadUserSettings, saveUserSettings } from "./ui/persistence";

M.glMatrix.setMatrixArrayType(Array)

const store = newStore()
const perfRef = {t0: performance.now()};
const printState = (st: typeof store) => () => {
  let t1 = performance.now();
  //console.log(st.getState())
  //console.log(`Update took ${t1 - perfRef.t0} ms.`);
}
setupEventsAndInit(store, perfRef)
//store.subscribe(printState(store))
store.subscribe(() => drawAll(store))

//Initialization
store.dispatch(addWeapon([0, 0, 0], "mortar"))
settingsToActions(loadUserSettings()).map(store.dispatch);
store.dispatch(newWriteAction("selectedWeapon", {type: "Weapon", id: 0}))
store.dispatch(changeMap("kokan"))

var fragment = new DocumentFragment();
mapOptions.forEach(o => {
  const imageLoc = o[2];
  var link = document.createElement('link')
  link.rel = "prefetch";
  link.as = "image";
  link.href = imageLoc;
  fragment.appendChild(link)
})
document.getElementById("preloadContainer")?.append(fragment);
/*
to be optimized:
- mouse movement triggers rerender
- 
use middleware to trigger canvas updates similar to replication?
*/

const $tooltip = document.getElementById('tooltip')!;
const $leftPanel = document.getElementById('leftPanel')!;
const $rightPanel = document.getElementById('rightPanel')!;
const h = React.createElement

ReactDOM.render(makeLeftPanel(store), $leftPanel)
//ReactDOM.render(makeRightPanel(store), $rightPanel)
ReactDOM.render(makeTooltip(store), $tooltip)
