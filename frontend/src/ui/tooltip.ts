import * as React from "react";
import { Provider, connect  } from 'react-redux'
import { createSession, join, sendMessage } from '../replication/actions';
import { canvas2world, event2canvas, world2keypadString } from "../world/transformations";
import { getHeight } from "../heightmap/heightmap";

const h = React.createElement
const div = <P, C>(props: P, children?: C) => h("div", props, children)

const tooltip: (props:any) => any  
  = props => {
    const worldPos = canvas2world(props.camera, props.uiState.mousePosition);
    const keypad = world2keypadString(props.minimap, worldPos)
    const height = getHeight(props.heightmap, worldPos)
    return h("div", {className: ""}, [
      `${keypad[0]}-${keypad[1]}-${keypad[2]} |  ${height/100}m`,
      //`pos: ${worldPos.map(Math.floor)}`
    ])
}

const mapStateToProps = (state: any /*, ownProps*/) => {
  return {
    uiState: state.uiState,
    minimap: state.minimap,
    heightmap: state.heightmap,
    camera: state.camera
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  newSession: () => dispatch(createSession()),
  join: (peerId: string) => dispatch(join(peerId)),
  sendMessage: (userId: string, message: string) => dispatch(sendMessage(userId, message)),
})

const connectedTooltip = connect(
  mapStateToProps,
  mapDispatchToProps
)(tooltip)

export const makeTooltip: (store: any) => any =
  store =>  h(Provider, {store},
    h(connectedTooltip)
  )
