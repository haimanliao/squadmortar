import { UserSettings, UserSettingsActionType as USAT, UIStateActionType as UIAT, UserSettingsAction, UIState, TouchInfo } from './types';
import {RecordOf, Record, Map } from 'immutable';
import { recordUpdateTransition, newRecordReducer, basicReducer } from '../common/reducer';
import { vec3 } from 'gl-matrix';

const defaultUserSettings = (): UserSettings => ({
    mapId: "albasrah",
    mapGrid: true,
    contourmap: false,

    weaponType: "standardMortar",
    selectedWeapon: null,
    extraWeaponHeight: 0,
    weaponPlacementHelper: true,
    
    fontSize: 16,
    targetSpread: false,
    targetSplash: false,
    targetGrid: false,
    targetDistance: false,
    targetPlacementHelper: false,

    extraButtonsAlwaysShown: false,
    deleteMode: false,

    targetCompactMode: false,
    leftPanelCollapsed: false,
});
const newUserSettings: Record.Factory<UserSettings> = Record(defaultUserSettings());
export const userSettings = newRecordReducer(USAT.write, newUserSettings, recordUpdateTransition)

const defaultUIState = (): UIState => ({
  dragEntityId: {type: "Camera", id: 0},
  dragStartPosition: vec3.fromValues(0, 0, 0),
  mousePosition: vec3.fromValues(0, 0, 0),
  mouseDown: false,
  touches: Map<any, TouchInfo>(),
})
const newUIState: Record.Factory<UIState> = Record(defaultUIState());
export const uiStateRecord = newRecordReducer(UIAT.write, newUIState, recordUpdateTransition)

export const uiState = basicReducer(newUIState, (state: any, action: any) => {
  switch (action.type) {
    case UIAT.write:
      return recordUpdateTransition(state, action.payload);
    case UIAT.updateTouch:
      return state.update("touches", (t: any) => t.set(action.payload.identifier, action.payload));
    case UIAT.removeTouch:
      return state.update("touches", (t: any) => t.remove(action.payload.identifier));
    default:
      return state

  }
})
