import * as React from "react";
import { Provider, connect  } from 'react-redux'
import { newWriteAction as writeUserSettings} from './actions';
import { Store0 } from '../store';
import { toggleButton } from '../common/toggleButton';
import { drawAll } from '../render/canvas';
import { useState } from 'react';
import { createSession, join, sendMessage } from '../replication/actions';
import { DataConnection } from "peerjs";

const h = React.createElement
const div = <P, C>(props: P, children?: C) => h("div", props, children)

const rightPanel: (props:any) => any  
  = props => {
    const [peerId, setPeerId] = useState("");
    return h("div", {className: "rightPanel flexItem"}, [
      div({className: "flexRow"}, [
        h("div", {className: "flexItem divButton", style:{width: "100%"},
          onClick: props.newSession
        }, "start p2p client")
      ]),
      div({className: "v3"}),
      div({className: "flexRow", style: {}}, [
        h("div", {className: "flexItemFill"}, props.peerId),
      ]),
      div({className: "v3"}),
      div({className: "flexRow", style: {justifyContent: "space-between"}}, [
        div({className: "flexItem", style: {height: "3em"}},
          h("textarea", {className:"peerIdInput", onChange: (e) => setPeerId((<HTMLTextAreaElement>e.target).value)})
        ),
      ]),
      div({className: "v3"}),
      div({className: "flexRow", style: {justifyContent: "space-between"}}, [
        
        h("div", {className: "flexItemFill divButton ", onClick: () => props.join(peerId)}, "join"),
        div({className: "h3"}),
        h("div", {className: "flexItemFill divButton"}, "leave"),
      ]),
      div({className: "v3"}),
      div({className: "flexRow", style: {}}, [
        h("div", {className: "flexItemFill divButton ", onClick: () => props.sendMessage(peerId, {ping: "ping!!111"})}, "ping"),
      ]),
      div({className: "flexRow", style: {}}, "Connections"),
      props.users?.map((c: string) => div({className: "flexRow green"}, c)),
      /*
      div({className: "white"}, "hello"),
      div({className: "green"}, "hello"),
      div({className: "yellow"}, "hello"),
      div({className: "red"}, "hello"),
      div({className: "blue"}, "hello"),
      */
  ])
}

const mapStateToProps = (state: any /*, ownProps*/) => {
  return {
    userSettings: state.userSettings,
    peerId: state.session?.get("id"),
    users: state.session?.get("users")?.keySeq(),
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  newSession: () => dispatch(createSession()),
  join: (peerId: string) => dispatch(join(peerId)),
  sendMessage: (userId: string, message: string) => dispatch(sendMessage(userId, message)),
})

const connectedRightPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(rightPanel)

export const makeRightPanel: (store: any) => any =
  store =>  h(Provider, {store},
    h(connectedRightPanel)
  )
