import { EntityId } from '../world/types';
import { dispatch, Store0 } from '../store';
import { newMousePosition, setMouseDown, setDragEntity, setDragStartPosition, updateTouch, removeTouch } from './actions';
import { changeZoom, moveCamera, setTransform } from '../camera/actions';
import { Camera } from '../camera/types';
import { vec3, mat4, quat } from 'gl-matrix';
import { camera } from '../camera/reducer';
import { drawAll } from '../render/canvas';
import { canvas2world, canvas2worldScale, event2canvas} from '../world/transformations';
import * as cam from "../camera/camera";
import { addWeapon } from '../world/weapon/actions';
import { addTarget, removeTarget } from '../world/target/actions';
import { getClosestEntity, getEntitiesWithin } from '../world/world';
import { TouchInfo, UIState } from './types';
import { moveEntityBy, moveEntityTo } from '../world/actions';
import { getActiveWeapon } from '../world/weapon/reducer';
import { $canvas, $contourmap } from '../elements';
import { getZoom } from '../camera/camera';


const dragOrPan = (store: Store0, event: any) => {
  const state = store.getState();
  const start = state.uiState.dragStartPosition;
  const eventLocation = canvas2world(state.camera, event2canvas(event));
  const dragEntityId = state.uiState.dragEntityId;
  const offset = vec3.sub(vec3.create(), eventLocation, start);

  if (dragEntityId.type == "Camera"){
    dispatch(store, moveCamera(offset))
  }
  else {
    dispatch(store, setDragStartPosition(eventLocation))
    dispatch(store, moveEntityBy(dragEntityId, offset))
  }
}

export const mouseMove = (store: Store0) => (e: MouseEvent) => {
  if ((e.buttons & 1) === 1){
    dragOrPan(store, e)
  }
  const mouseXY = event2canvas(e);
  dispatch(store, newMousePosition(mouseXY[0], mouseXY[1]))
}

const zoom = (store: Store0, targetElement: any, zoomLocation: vec3, desiredZoom: number) => {
  const camera = store.getState().camera;
  const newZoom = Math.max(0.002, Math.min(0.08, desiredZoom))
  $contourmap.set_zoom(newZoom);
  dispatch(store, changeZoom(event2canvas({target: targetElement, clientX: zoomLocation[0], clientY: zoomLocation[1]}), newZoom));
}

export const mouseScroll = (store: Store0) => (e: 	WheelEvent) => {
  const camera = store.getState().camera;
  const currentZoom = getZoom(store.getState().camera)
  let delta = currentZoom < 0.01 ? 0.001 : currentZoom < 0.02 ? 0.002 : 0.005;
  const newZoom = e.deltaY > 0 ?  Math.max(0.002, currentZoom - delta) : Math.min(0.08, currentZoom + delta)
  zoom(store, $canvas, event2canvas(e), newZoom)
}

export const mouseDown = (store: Store0) => (e: MouseEvent) => {
  const dragEntity = getDragEntity(store)(e)
  const eventLocation = canvas2world(store.getState().camera, event2canvas(e))
  dispatch(store, setDragStartPosition(eventLocation))
  dispatch(store, setDragEntity(dragEntity))
  dispatch(store, setMouseDown(true))
}

export const mouseUp = (store: Store0) => (e: any) => {
  dispatch(store, setMouseDown(false))
}

export const click = (store: Store0) => (e: any) => {
  if (e.shiftKey){
    const state = store.getState();
    const eventLocation = canvas2world(state.camera, event2canvas(e))
    const wpn = getActiveWeapon(state.userSettings, state.entities.Weapon)
    if (wpn !== null){
      dispatch(store, moveEntityTo(wpn.entityId, eventLocation))
    }
    //dispatch(store, addWeapon(eventLocation, "mortar"))
  }
  if (e.ctrlKey || store.getState().userSettings.deleteMode){
    const state = store.getState();
    const worldLoc = canvas2world(state.camera, event2canvas(e))
    const radius = canvas2worldScale(state.camera, [25, 0, 0])[0]
    const candidates = getClosestEntity(state.entities, worldLoc, radius)

    if (candidates.length > 0){
      dispatch(store,  removeTarget(candidates[0].entityId));
    }
  }
}

export const doubleClick = (store: Store0) => (e: any) => {
  const eventLocation = canvas2world(store.getState().camera, event2canvas(e))
  dispatch(store, addTarget(eventLocation))
}

export const getDragEntity: (store: Store0) => (event: any) => EntityId
  = store => event => {
    const state = store.getState();
    const worldLoc = canvas2world(state.camera, event2canvas(event));
    const radius = canvas2worldScale(state.camera, vec3.fromValues(25, 0, 0))[0]
    const candidates = getClosestEntity(state.entities, worldLoc, radius)
    if (candidates.length > 0){
      return candidates[0].entityId;
    }
    return {type: "Camera", id: 0}
}

export const handleNewTouch = (store: Store0) => (ev: any) => {
  // not preventing default to keep click emulation intact
  const range = Array(ev.changedTouches.length).fill(0).map((x, y) => x + y)
  range
    .map(k => {
      let touch = ev.changedTouches[k]
      store.dispatch(updateTouch(touch.identifier, touch.clientX, touch.clientY))
    })
    if (ev.changedTouches.length === 1) {
      let touchEvent = ev.changedTouches[0]
      const dragEntity = getDragEntity(store)(touchEvent)
      const eventLocation = canvas2world(store.getState().camera, event2canvas(touchEvent))
      dispatch(store, setDragStartPosition(eventLocation))
      dispatch(store, setDragEntity(dragEntity))
    }
}

const pinch = (store: Store0) => (ev: any) => {
  const eventTouches = Object.values<Touch>(ev.changedTouches);
  let knownTouches = Array.from(store.getState().uiState.touches.values()).sort((a:any, b:any) => a.identifier - b.identifier) as Array<TouchInfo>;
  let trackedTouches = knownTouches.slice(0, 2)
  let oldDist = vec3.distance(trackedTouches[0].location, trackedTouches[1].location)
  let zoomLocation = vec3.create();
  vec3.add(zoomLocation, trackedTouches[0].location, trackedTouches[1].location)
  vec3.scale(zoomLocation, zoomLocation, 0.5)
  let factorIncrease = 0
  let target: any = null
  eventTouches.forEach((eventTouch: Touch) => {
    trackedTouches.forEach((trackedTouch: any) => {
      if (eventTouch.identifier === trackedTouch.identifier){
        target = eventTouch.target;
        factorIncrease = factorIncrease + 2 * vec3.distance(vec3.fromValues(eventTouch.clientX, eventTouch.clientY, 0), zoomLocation) / oldDist - 1
      }
    })
  })
  let zoomChangeFactor = 1 + factorIncrease
  if (zoomChangeFactor > 0 && target !== null){
    const currentZoom = getZoom(store.getState().camera)
    zoom(store, $canvas, zoomLocation, currentZoom * zoomChangeFactor)
  }
}

const touchMove = (store: Store0) => (ev: any) => {
  const eventTouches = Object.values<Touch>(ev.changedTouches);
  let knownTouches = Array.from(store.getState().uiState.touches.values()).sort((a:any, b:any) => a.identifier - b.identifier) as Array<TouchInfo>;
  if (knownTouches.length === 1) {
    const touchEvent = eventTouches[0]
    dragOrPan(store, touchEvent)
  }
}
export const handleTouchMove = (store: Store0) => (ev: any) => {
  ev.preventDefault();
  ev.stopImmediatePropagation();
  const eventTouches = Object.values<Touch>(ev.changedTouches);
  let knownTouches = Array.from(store.getState().uiState.touches.values()).sort((a:any, b:any) => a.identifier - b.identifier) as Array<TouchInfo>;
  if (knownTouches.length >= 2) {
    pinch(store)(ev);
  } else if (eventTouches) {
    touchMove(store)(ev)
  }
  eventTouches.forEach((t: Touch) => {
    store.dispatch(updateTouch(t.identifier, t.clientX, t.clientY))
  })
}

export const handleTouchEnd = (store: Store0) => (ev: any) => {
  const range = Array(ev.changedTouches.length).fill(0).map((x, y) => x + y)
  range.forEach(k => store.dispatch(removeTouch(ev.changedTouches[k].identifier)));
}