import { mat4, vec3 } from "gl-matrix"
import {world2heightmap } from "../world/transformations";
import { $imagedataCache } from "./reducer";
import { Heightmap } from "./types"

export const getHeight = (heightmap: Heightmap, worldLoc: vec3) => {
  try {
    const heightmapPos = world2heightmap(heightmap, worldLoc).map(Math.floor);
    const dataIndex = (heightmapPos[1] * heightmap.size[0] + heightmapPos[0]) * 4
    if ($imagedataCache){
      const data = ($imagedataCache as any).data[dataIndex];
      const scale_z = mat4.getScaling(vec3.create(), heightmap.texture.transform)[2]
      return scale_z * data * 2; // forgot why *2, probably because of 16bit greyscale loaded as 8bit rgba
    }
    //imgdata?.data[dataIndex] || -1;
    return 0
  } catch (error) {
    return 0
  }
}