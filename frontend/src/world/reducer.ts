//import { EntityActionType, EntityRegistry } from './types';
import { Record, List, Map } from 'immutable';
//import { setEntity, removeEntity, updateEntity } from './world';
import { CombinedState, combineReducers, Reducer } from 'redux';
import { targetReducer } from './target/reducer';
import { weaponReducer } from './weapon/reducer';


/*
const defaultRegistry: () => EntityRegistry = 
  Record({
    Camera: Map(),
    Weapon: Map(),
    WeaponTarget: Map()
  })

export const entityRegistry: (state: any, action: any) => EntityRegistry =
  (state, action) => {
    if (state === undefined){
        return defaultRegistry();
    }
    switch(action.type){
      case EntityActionType.add:
        return setEntity(state, action.payload);
      case EntityActionType.remove:
        return removeEntity(state, action.payload);
      case EntityActionType.update:
        return updateEntity(state, action.payload.eid, action.payload.update);
      default:
        return state;
    }
  }

  */


 export const entityRegistry = combineReducers({
   Weapon: weaponReducer,
   Target: targetReducer
 })

