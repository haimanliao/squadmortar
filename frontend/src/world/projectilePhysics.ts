import { vec3 } from "gl-matrix";
import { GRAVITY } from "./constants";


export function calcAngle(x: number, startHeightOffset: number, v: number, g: number) {
  // https://en.wikipedia.org/wiki/Projectile_motion
  // -> Angle {\displaystyle \theta } \theta required to hit coordinate (x,y)
  const y = - startHeightOffset;
  const d = Math.sqrt(v ** 4 - g * (g * x ** 2 + 2 * y * v ** 2));
  const rad = Math.atan((v ** 2 + d) / (g * x));
  return rad;
}

function calcAngleLow(x: number, startHeightOffset: number, v: number, g: number) {
  // https://en.wikipedia.org/wiki/Projectile_motion
  // -> Angle {\displaystyle \theta } \theta required to hit coordinate (x,y)
  const y = - startHeightOffset;
  const d = Math.sqrt(v ** 4 - g * (g * x ** 2 + 2 * y * v ** 2));
  const rad = Math.atan((v ** 2 - d) / (g * x));
  return rad;
}

export function solveProjectileFlight(x0: number, y0: number, z0: number, x1: number, y1: number, z1: number, velocity: number): [number, number, number] {
  //console.log("calc log", x0, x0, z0, x1, y1, z1, velocity)
  const dx = x1 - x0;
  const dy = y1 - y0;
  const dist2d = Math.round(Math.hypot(dx, dy));
  const dir = Math.round((Math.atan2(dx, -dy) * 180 / Math.PI + 360) % 360 * 100) /100;
  const angle = calcAngle(dist2d, z0 - z1, velocity, GRAVITY);
  return [angle, dir, dist2d];
}

export function distDir(vec1: vec3, vec2: vec3){
  const dx = vec2[0] - vec1[0];
  const dy = vec2[1] - vec1[1];
  const dist2d = Math.round(Math.hypot(dx, dy));
  const dir = Math.round((Math.atan2(dx, -dy) * 180 / Math.PI + 360) % 360 * 100) /100;
  return [dist2d, dir]
}

export function angle2groundDistance(angle: number, startHeightOffset: number, velocity: number): number {
  // distance over ground - for map drawing purposes
  const d = Math.sqrt(velocity ** 2 * Math.sin(angle) ** 2 + 2 * GRAVITY * startHeightOffset);
  if (isNaN(d)){ // cannot reach this height
    return 0;
  } else {
    return velocity * Math.cos(angle) * (velocity * Math.sin(angle) + d)/GRAVITY;
  }
}

export function flightTime(angle: number, startHeightOffset: number, velocity: number): number{
  const heightComponent =
    Math.sqrt(
      (velocity * Math.sin(angle)) ** 2
      + 2 * GRAVITY * startHeightOffset
     );
  return (velocity * Math.sin(angle) + heightComponent ) / GRAVITY
}

export function calcSpread(dist: number, startHeightOffset: number, velocity: number, deviation: number): [number, number, number] {
  const centerAngle = calcAngle(dist, startHeightOffset, velocity, GRAVITY);
  const close = angle2groundDistance(centerAngle + deviation, startHeightOffset, velocity)
  const far = angle2groundDistance(centerAngle - deviation, startHeightOffset, velocity)
  // i'm too lazy for the true horizontal component so i'll approximate it via
  // time of (accurate) flight and max horizontal deviation speed - should be close enough for small deviation angles.
  // ^ essentially linear approximation of angle change
  const horizontalSpeed = Math.sin(deviation) * velocity;
  return [horizontalSpeed * flightTime(centerAngle, startHeightOffset, velocity), dist - close, far - dist];
}