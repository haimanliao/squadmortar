import { vec3 } from 'gl-matrix';
import { EntityActionType } from '../types';
import { WeaponType, WeaponAction, WeaponActionType } from './types';

export const addWeapon: (location: vec3, weaponType: WeaponType) => WeaponAction = 
  (location, weaponType) => ({
    type: EntityActionType.add,
    payload: {location, entityType: "Weapon"}
  })