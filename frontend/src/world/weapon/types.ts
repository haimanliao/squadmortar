import { RecordOf } from 'immutable';
import { EntityAction, EntityActionType, EntityId, HasEntityId, HasTransform, TransformAction, TransformActionType } from '../types';
import { vec3 } from 'gl-matrix';

export type Weapon =  RecordOf<HasTransform & HasEntityId & {
  weaponType: WeaponType;
}>

export type WeaponType = "mortar" | "rocket"

export enum WeaponActionType {
}

export type WeaponAction 
  = EntityAction
  | TransformAction
