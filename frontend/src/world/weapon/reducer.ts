import { Map, RecordOf, Record } from 'immutable';
import { Weapon, WeaponAction, WeaponActionType, WeaponType } from './types';
import { EntityActionType, EntityId, EntityType, TransformActionType } from '../types';
import { vec3, mat4 } from 'gl-matrix';
import * as Transformation from '../transformations';
import { Maybe } from '../../common/types';
import { Store0 } from '../../store';
import { UserSettings } from '../../ui/types';


export type Weapons = RecordOf<{
  entities: Map<number, Weapon>;
  nextId: number;
}>

const newState = Record({
  entities: Map<number, Weapon>(),
  nextId: 0
})

export const weaponReducer: (state: Weapons, action: WeaponAction) => Weapons =
  (state, action) => {
    if (state === undefined){
        return newState();
    }
    switch(action.type){
      case EntityActionType.add:
        return action.payload.entityType === "Weapon"
          ? addNewWeapon(state, action.payload.location)
          : state
      case TransformActionType.moveBy:
        return action.payload.entityId.type === "Weapon"
          ? moveBy(state, action.payload.entityId, action.payload.vector)
          : state;
      case TransformActionType.moveTo:
        return action.payload.entityId.type === "Weapon"
          ? moveTo(state, action.payload.entityId, action.payload.location)
          : state;
      default:
        return state;
    }
  }

const WeaponRecord = Record({
  entityId: {type: "Weapon" as EntityType, id: 0},
  transform: mat4.create(),
  weaponType: "mortar" as WeaponType
})

const newWeapon: (id: number, location: vec3, weaponType: WeaponType) => Weapon = 
  (id, location, weaponType) => WeaponRecord({
    entityId: {type: "Weapon", id},
    transform: mat4.fromTranslation(mat4.create(), location),
    weaponType
  })

const addNewWeapon: (state: Weapons, location: vec3) => Weapons =
  (state, location) => {
    const nextId = state.get("nextId");
    return state
      .update("nextId", n => n + 1)
      .update("entities", e => e.set(nextId, newWeapon(nextId, location, "mortar")))
  }
    
const moveBy: (state: Weapons, entityId: EntityId, offset: vec3) => Weapons =
(state, entityId, offset) => {
  return state
    .update("entities", 
      e => e.update(entityId.id, 
        t => t.update("transform", Transformation.moveBy(offset))))
}

const moveTo: (state: Weapons, entityId: EntityId, newLocation: vec3) => Weapons =
(state, entityId, newLocation) => {
  return state
    .update("entities", 
      e => e.update(entityId.id, 
        t => t.update("transform", Transformation.moveTo(newLocation))))
}

export const getActiveWeapon: (userSettings: UserSettings, weapons: Weapons) => Maybe<Weapon> = 
  (userSettings, weapons) => {
    if (userSettings.selectedWeapon !== null){
      return weapons.get("entities").get(userSettings.selectedWeapon.id, null)
    }
    return null
  }