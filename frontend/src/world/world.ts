import { mat4, vec3 } from "gl-matrix";
import { Store0 } from "../store";
import { Entities, HasEntityId, HasLocation, HasTransform } from "./types";

export const getEntitiesWithin = (entities: Entities, worldLoc: vec3, radius: number): Array<HasEntityId & HasTransform> => {
  const entityInRange = (entity: HasTransform) => {
    const entityLoc = mat4.getTranslation(vec3.create(), entity.transform)
    return vec3.distance(worldLoc, entityLoc);
  }
  const out: any = [];
  out.push(entities.Target.entities.valueSeq().filter(entityInRange))
  out.push(entities.Weapon.entities.valueSeq().filter(entityInRange))
  return out;
}

export const getClosestEntity = (entities: Entities, worldLoc: vec3, radius: number): Array<HasEntityId & HasTransform> => {
  let minDist = radius;
  let out:any = [];
  const entitySets = [
    entities.Target.entities.valueSeq(),
    entities.Weapon.entities.valueSeq()
  ]
  entitySets.forEach(elem => elem.forEach(
    (entity:any) => {
      const entityLoc = mat4.getTranslation(vec3.create(), entity.transform);
      const curDist = vec3.distance(worldLoc, entityLoc)
      if (curDist < minDist){
        minDist = curDist;
        out = [entity]
      }
    })
  );
  return out;
}




/*

import { Weapon,EntityId, HasEntityId} from './types';
import { List, Map, RecordOf } from 'immutable';

export const setEntity: <E extends RecordOf<HasEntityId>>(er: EntityRegistry, e: E) => EntityRegistry =
  (er, e) => er.update(e.get("entityId").type, (m: Map<number, any>) => m.set(e.get("entityId").id, e))

export const removeEntity:  <E>(er: EntityRegistry, e: EntityId) => EntityRegistry =
  (er, e) => er.update(e.type, (m: Map<number, any>) => m.remove(e.id))

export const updateEntity: <E>(er: EntityRegistry, ei: EntityId, updater:(e: E) => E) => EntityRegistry =
  (er, ei, updater) => er.update(ei.type, (m: Map<number, any>) => m.update(ei.id, updater))


*/