
import { vec3, mat4 } from 'gl-matrix';
import { Map, List, RecordOf } from 'immutable';
import { Camera } from '../camera/types';
import { Targets } from './target/reducer';
import { Weapons } from './weapon/reducer';


export type EntityType = "Camera" | "Weapon" | "Target"
export type EntityId = {type: EntityType, id: number}
export type HasEntityId = {entityId: EntityId}

export type HasTransform = {transform: Transform}
export type Transform = mat4 //HasLocation & HasRotation & HasScale;

export type HasLocation = {location: vec3}
export type HasRotation = {rotation: vec3}
export type HasScale = {scale: vec3}
export type WorldArea = {
  
}
export type Entities = {
  Weapon: Weapons,
  Target: Targets
}
export type WeaponTarget = RecordOf<{}>

export enum EntityActionType {
  add     = "ENTITY_ADD",
  remove  = "ENTITY_REMOVE",
}

export type EntityAction = 
    {type: EntityActionType.add, payload: HasLocation & {entityType: EntityType}}
  | {type: EntityActionType.remove, payload: HasEntityId}


export enum TransformActionType {
  moveTo = "TRANSFORM_MOVE_TO",
  moveBy = "TRANSFORM_MOVE_BY"
}

export type TransformAction = 
    {type: TransformActionType.moveTo, payload: {entityId: EntityId, location: vec3}}
  | {type: TransformActionType.moveBy, payload: {entityId: EntityId, vector: vec3}}

/*
export type EntityRegistry = RecordOf<{
  Camera: Map<number, Camera>;
  Weapon: Map<number, Weapon>;
  WeaponTarget: Map<number, WeaponTarget>;
}>
*/

export type EntitySet = RecordOf<{
  entities: number
}>
