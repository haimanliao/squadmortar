import { RecordOf } from 'immutable';
import { EntityAction, HasEntityId, HasTransform, TransformAction } from '../types';

export type Target =  RecordOf<HasTransform & HasEntityId & {
}>
export enum TargetActionType {
  removeAll = "TARGET_REMOVE_ALL"
}
export type TargetAction 
  = EntityAction
  | TransformAction
  | {type: TargetActionType.removeAll, payload: {}}
