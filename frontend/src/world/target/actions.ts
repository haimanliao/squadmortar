import { vec3 } from 'gl-matrix';
import { EntityActionType, EntityId } from '../types';
import {TargetAction, TargetActionType } from './types';

export const addTarget: (location: vec3) => TargetAction = 
  (location) => ({
    type: EntityActionType.add,
    payload: {location, entityType: "Target"}
})

export const removeTarget: (entityId: EntityId) => TargetAction = 
  (entityId) => ({
    type: EntityActionType.remove,
    payload: {entityId}
})

export const removeAllTargets: () => TargetAction = 
  () => ({
    type: TargetActionType.removeAll,
    payload: {}
})