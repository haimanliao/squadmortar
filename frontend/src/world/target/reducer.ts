import { Map, RecordOf, Record } from 'immutable';
import { Target, TargetAction, TargetActionType } from './types';
import { EntityActionType, EntityId, EntityType, TransformActionType } from '../types';
import { vec3, mat4 } from 'gl-matrix';
import * as Transformation from '../transformations';


export type Targets = RecordOf<{
  entities: Map<number, Target>;
  nextId: number;
}>

const newState = Record({
  entities: Map<number, Target>(),
  nextId: 0
})

export const targetReducer: (state: Targets, action: TargetAction) => Targets =
  (state, action) => {
    if (state === undefined){
        return newState();
    }
    switch(action.type){
      case EntityActionType.add:
        return action.payload.entityType === "Target"
          ? addNewTarget(state, action.payload.location)
          : state;
      case EntityActionType.remove:
        return action.payload.entityId.type === "Target"
          ? removeTarget(state, action.payload.entityId)
          : state;
      case TransformActionType.moveBy:
        return action.payload.entityId.type === "Target"
          ? moveBy(state, action.payload.entityId, action.payload.vector)
          : state;
      case TargetActionType.removeAll:
        return removeAllTargets(state);
      default:
        return state;
    }
  }

const TargetRecord = Record({
  entityId: {type: "Target" as EntityType, id: 0},
  transform: mat4.create(),

})

const newTarget: (id: number, location: vec3) => Target = 
  (id, location) => TargetRecord({
    entityId: {type: "Target", id},
    transform: mat4.fromTranslation(mat4.create(), location),
  })

const addNewTarget: (state: Targets, location: vec3) => Targets =
  (state, location) => {
    const nextId = state.get("nextId");
    return state
      .update("nextId", n => n + 1)
      .update("entities", e => e.set(nextId, newTarget(nextId, location)))
  }

const removeTarget: (state: Targets, entityId: EntityId) => Targets =
  (state, entityId) => {
    return state
      .update("entities", e => e.delete(entityId.id))
  }

const removeAllTargets: (state: Targets) => Targets =
  (state) => {
    const nextId = state.get("nextId");
    return state
      .update("nextId", n => 0)
      .update("entities", e => Map<number, Target>())
  }
    
const moveBy: (state: Targets, entityId: EntityId, offset: vec3) => Targets =
  (state, entityId, offset) => {
    return state
      .update("entities", 
        e => e.update(entityId.id, 
          t => t.update("transform", Transformation.moveBy(offset))))
  }

export const getTarget: (state: Targets, entityId: EntityId, offset: vec3) => Targets =
(state, entityId, offset) => {
  return state
    .update("entities", 
      e => e.update(entityId.id, 
        t => t.update("transform", Transformation.moveBy(offset))))
  }