import { vec3 } from "gl-matrix";
import { EntityAction, EntityActionType, EntityId, TransformAction, TransformActionType } from "./types";

export const removeEntity: (entityId: EntityId) => EntityAction = 
(entityId) => ({
  type: EntityActionType.remove,
  payload: {entityId}
})

export const moveEntityBy: (entityId: EntityId, vector: vec3) => TransformAction = 
(entityId, vector) => ({
  type: TransformActionType.moveBy,
  payload: {entityId, vector}
})

export const moveEntityTo: (entityId: EntityId, location: vec3) => TransformAction = 
(entityId, location) => ({
  type: TransformActionType.moveTo,
  payload: {entityId, location}
})