import { Camera } from "../camera/types"
import { Weapon } from '../world/weapon/types';
import { Weapons } from '../world/weapon/reducer';
import { applyTransform } from "../world/transformations";
import { canvasScaleTransform } from "./canvas";
import { Heightmap } from "../heightmap/types";
import { MORTAR_MIN_RANGE, MORTAR_MAX_RANGE } from "../world/constants";
import { mat4, vec3 } from "gl-matrix";

export const drawWeapons: (ctx: CanvasRenderingContext2D, camera:Camera,  weaponSet: Weapons) => void = 
  (ctx, camera, weaponSet) => {
    const drawWeapon = (ctx: any, wpn: Weapon) => {
      ctx.save()
      
      const canvasTransform = canvasScaleTransform(camera) 
      const scale = mat4.getScaling(vec3.create(), canvasTransform)[0]

      applyTransform(ctx, wpn.get("transform"))
      ctx.beginPath();
      ctx.lineWidth = 1 * scale;
      ctx.strokeStyle = '#0f0';
      ctx.arc(0, 0, MORTAR_MIN_RANGE, 0, 2 * Math.PI);
      ctx.stroke();

      ctx.beginPath();
      ctx.lineWidth = 1 * scale;
      ctx.strokeStyle = '#0f0';
      ctx.arc(0, 0, MORTAR_MAX_RANGE, 0, 2 * Math.PI);
      ctx.stroke();

      applyTransform(ctx, canvasTransform)

      ctx.beginPath();
      ctx.lineWidth = 3
      ctx.strokeStyle = 'black';
      ctx.arc(0, 0, 5, 0, 2 * Math.PI);
      ctx.stroke();
      ctx.beginPath();
      ctx.lineWidth = 1
      ctx.strokeStyle = '#0f0';
      ctx.arc(0, 0, 5, 0, 2 * Math.PI);
      ctx.stroke();

      ctx.restore()
    }
    weaponSet.get("entities").forEach((v: Weapon) => drawWeapon(ctx, v))
  }