import { Camera } from "../camera/types"
import { applyTransform, getTranslation, newTranslation, world2heightmap } from "../world/transformations";
import { canvasScaleTransform, drawLine, drawSpreadEllipse, outlineText } from "./canvas";
import { Target } from "../world/target/types";
import { Targets } from '../world/target/reducer';
import { mat4, vec3 } from "gl-matrix";
import { Weapon } from "../world/weapon/types";
import { Heightmap } from "../heightmap/types";
import { Weapons } from "../world/weapon/reducer";
import { EntityId } from "../world/types";
import { Maybe } from "../common/types";
import { getHeight } from "../heightmap/heightmap";
import { angle2groundDistance, calcSpread, distDir, solveProjectileFlight } from "../world/projectilePhysics";
import {MAPSCALE, MORTAR_100_DAMAGE_RANGE, MORTAR_25_DAMAGE_RANGE, MORTAR_DEVIATION, MORTAR_VELOCITY, US_MIL} from "../world/constants";
import { userSettings } from "../ui/reducer";
import { UserSettings } from "../ui/types";
import { $s5map } from "../elements";
import { TEXT_RED, TEXT_WHITE } from "./constants";

function drawMortarGridLine(ctx: CanvasRenderingContext2D, x0: number, y0: number, r0: number, r1: number, dir: number) {
  let phi = dir * Math.PI / 180;
  let [kx, ky] = [Math.sin(phi), -Math.cos(phi)];
  drawLine(ctx, x0 + kx * r0, y0 + ky * r0, x0 + kx * r1, y0 + ky * r1);
}
function drawMortarGridArc(ctx: CanvasRenderingContext2D, x0: number, y0: number, r: number, dir: number) {
  if (r >= 0){
    let alpha = Math.PI / 180;
    let phi = (dir - 90) * Math.PI / 180;
    ctx.beginPath();
    ctx.arc(x0, y0, r, phi - 2 * alpha, phi + 3 * alpha);
    ctx.stroke();
  }
}

export const drawTargets: (ctx: CanvasRenderingContext2D, camera:Camera, userSettings: UserSettings, heightmap: Heightmap, activeWeapon: Maybe<Weapon>, targetSet: Targets) => void = 
  (ctx, camera, userSettings, heightmap, activeWeapon, targetSet) => {
    if (activeWeapon !== null) {
      const weaponTranslation = getTranslation(activeWeapon.transform);
      const weaponHeight = getHeight(heightmap, weaponTranslation)
      weaponTranslation[2] = weaponHeight + userSettings.extraWeaponHeight * 100;

      if (userSettings.weaponType === "standardMortar" || userSettings.weaponType === "technicalMortar"){
        targetSet.get("entities").forEach((t: Target) => drawMortarTarget(ctx, camera, userSettings, weaponTranslation, heightmap, activeWeapon, t))
      } else if (userSettings.weaponType === "rocket"){
        targetSet.get("entities").forEach((t: Target) => drawRocketPodTarget(ctx, camera, userSettings, weaponTranslation, heightmap, activeWeapon, t))
      }
    }
  }

const drawMortarTarget = (ctx: any, camera:Camera, userSettings: UserSettings, weaponTranslation: vec3, heightmap: Heightmap, activeWeapon: Weapon, target: Target) => {
  const canvasSizeFactor = mat4.getScaling(vec3.create(), canvasScaleTransform(camera))[0] // uniform scaling

  if (weaponTranslation !== null &&  weaponTranslation[2] !== null) {
    const targetTranslation = getTranslation(target.transform);
    const targetHeight = getHeight(heightmap, targetTranslation)
    targetTranslation[2] = targetHeight;
    const startHeightOffset = weaponTranslation[2] - targetHeight;

    const [angle, dir, dist] = solveProjectileFlight(
      weaponTranslation[0], weaponTranslation[1], weaponTranslation[2],
      targetTranslation[0], targetTranslation[1], targetTranslation[2],
      MORTAR_VELOCITY
    );
    const mil = angle * US_MIL;
    const milCapped = mil < 800 ? 0 : mil;
    const milRounded = Math.floor(milCapped*10)/10;

    if (userSettings.targetGrid && activeWeapon !== null){
      ctx.save()
      applyTransform(ctx, activeWeapon.get("transform"))
      const gridDir = Math.floor(dir);
      const mil5 = Math.floor(milRounded / 5) * 5;
      ctx.strokeStyle = '#0f0';
      ctx.lineWidth = 1 * canvasSizeFactor;

      const arcRadii = [-10, -5, 0, 5, 10, 15].map(
        x => angle2groundDistance((mil5 + x)/US_MIL, startHeightOffset, MORTAR_VELOCITY)
      );
      //console.log("angle", angle,"angle/US_MIL", angle/US_MIL, "mil5", mil5, "ar", arcRadii)
      const [ra, r0, r1, r2, r3, rb] = arcRadii;
      [-2, -1, 0, 1, 2, 3].forEach(
        gridOffset => drawMortarGridLine(ctx, 0, 0, ra, rb, gridDir + gridOffset)
      )
      arcRadii.forEach(
        arcRadius => drawMortarGridArc(ctx, 0, 0, arcRadius, gridDir)
      )
      ctx.restore()
    }

    ctx.save()
    applyTransform(ctx, target.get("transform"))
    if (userSettings.targetSpread){
      const [hSpread, closeSpread, farSpread] = calcSpread(dist, startHeightOffset, MORTAR_VELOCITY, MORTAR_DEVIATION)
      //console.log("spread", hSpread, closeSpread, farSpread)
      ctx.lineWidth = 1 * canvasSizeFactor
      ctx.strokeStyle = '#00f';
      drawSpreadEllipse(ctx, vec3.subtract(vec3.create(), weaponTranslation, targetTranslation), hSpread, closeSpread, closeSpread)
      if (userSettings.targetSplash){
        ctx.strokeStyle = '#f00';
        drawSpreadEllipse(ctx, vec3.subtract(vec3.create(), weaponTranslation, targetTranslation), hSpread + MORTAR_100_DAMAGE_RANGE, closeSpread + MORTAR_100_DAMAGE_RANGE, closeSpread + MORTAR_100_DAMAGE_RANGE)
        drawSpreadEllipse(ctx, vec3.subtract(vec3.create(), weaponTranslation, targetTranslation), hSpread + MORTAR_25_DAMAGE_RANGE, closeSpread + MORTAR_25_DAMAGE_RANGE, closeSpread + MORTAR_25_DAMAGE_RANGE)
      }
    } else if (userSettings.targetSplash){
      ctx.lineWidth = 1 * canvasSizeFactor
      ctx.strokeStyle = '#f00';
      ctx.beginPath();
      ctx.arc(0, 0, MORTAR_100_DAMAGE_RANGE, 0, 2 * Math.PI);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(0, 0, MORTAR_25_DAMAGE_RANGE, 0, 2 * Math.PI);
      ctx.stroke();
    }

    // firing solution text
    applyTransform(ctx, canvasScaleTransform(camera))
    const angleValue = userSettings.weaponType === "technicalMortar" ? angle / Math.PI * 180  : milRounded;
    
    applyTransform(ctx, newTranslation(10, 0, 0))
    if (userSettings.targetCompactMode){
      let angleText =  "----.-"
      const angleValuePrecision = userSettings.weaponType === "technicalMortar" ? 1 : 0
      if (angle && angleValue >= 1000){
        angleText = angleValue.toFixed(angleValuePrecision).toString().substr(1, 4 + angleValuePrecision)
      } else if (angle) {
        angleText = angleValue.toFixed(angleValuePrecision).toString().substr(0, 3 + angleValuePrecision)
      }
      outlineText(ctx, angleText, "middle", TEXT_RED, TEXT_WHITE, userSettings.fontSize, true)
    } else {
      const angleText = angle ? `${(angleValue.toFixed(1))}` : "----.-"
      outlineText(ctx, angleText, "bottom", TEXT_RED, TEXT_WHITE,  userSettings.fontSize, true)
      const bottomText = userSettings.targetDistance ? `${dir.toFixed(1)}° ${(dist * MAPSCALE).toFixed(0)}m` : `${dir.toFixed(1)}°`
      outlineText(ctx, bottomText, "top", TEXT_RED, TEXT_WHITE, userSettings.fontSize * 2 / 3, true)
    }
    ctx.restore()
  }
  
  ctx.save()
  applyTransform(ctx, target.get("transform"))
  applyTransform(ctx, canvasScaleTransform(camera))
  
  ctx.beginPath();
  ctx.lineWidth = 3
  ctx.strokeStyle = 'black';
  ctx.arc(0, 0, 4, 0, 2 * Math.PI);
  ctx.stroke();

  ctx.beginPath();
  ctx.lineWidth = 1
  ctx.strokeStyle = 'red';
  ctx.arc(0, 0, 4, 0, 2 * Math.PI);
  ctx.stroke();
  ctx.restore()
}

function drawRocketPodTarget(ctx: any, camera:Camera, userSettings: UserSettings, weaponTranslation: vec3, heightmap: Heightmap, activeWeapon: Weapon, target: Target){
    const table = $s5map;
    const targetTranslation = getTranslation(target.transform);
    const targetHeight = getHeight(heightmap, targetTranslation)
    targetTranslation[2] = targetHeight;
    const startHeightOffset = weaponTranslation[2] - targetHeight;
    const [dist, dir] = distDir(weaponTranslation, targetTranslation);

    const angle = table.getAngle(dist, startHeightOffset)
    const time = table.getTime(dist, startHeightOffset)

    const hSpread = table.calcSpreadHorizontal(dist, startHeightOffset)
    let spread = table.calcSpreadVertical(dist, startHeightOffset) // "temp" hack...
    let closeSpread = spread[0];
    let farSpread = spread[1]
    closeSpread = closeSpread != 0 ? closeSpread : dist;
    const canvasSizeFactor = mat4.getScaling(vec3.create(), canvasScaleTransform(camera))[0] // uniform scaling
    
    ctx.save()
    applyTransform(ctx, target.get("transform"))
    if (userSettings.targetSpread && angle && time) {
      ctx.strokeStyle = '#00f';
      ctx.lineWidth = 1 * canvasSizeFactor;
      drawSpreadEllipse(ctx, vec3.subtract(vec3.create(), weaponTranslation, targetTranslation), hSpread, closeSpread, farSpread)
    }
    // firing solution text
    applyTransform(ctx, canvasScaleTransform(camera))
    applyTransform(ctx, newTranslation(15, 0, 0))
    
    const mil = angle * US_MIL;
    const degrees = angle * 180 / Math.PI;
    const milCapped = mil < 800 ? 0 : mil;
    const milRounded = Math.round(milCapped*10)/10;
    //drawText(canvasCtx, `${dir.toFixed(1)}\u00B0  ${Math.round(dist*10*MAPSCALE)/10}m`, text_x, text_y, 'bottom');
    if (angle && time){
      outlineText(ctx, `${degrees.toFixed(1)}°  ${time.toFixed(1)}s`, "bottom", TEXT_RED, TEXT_WHITE, userSettings.fontSize, true);
    } else {
      outlineText(ctx, `No firing solution`, "bottom", TEXT_RED, TEXT_WHITE, userSettings.fontSize * 2/3, true);
    }
    const bottomText = userSettings.targetDistance ? `${dir.toFixed(1)}° ${(dist * MAPSCALE).toFixed(0)}m` : `${dir.toFixed(1)}°`;
    outlineText(ctx, bottomText, "top", TEXT_RED, TEXT_WHITE, userSettings.fontSize * 2 / 3, true);
    ctx.restore()

    ctx.save()
    applyTransform(ctx, target.get("transform"))
    applyTransform(ctx, canvasScaleTransform(camera))
    
    ctx.beginPath();
    ctx.lineWidth = 3
    ctx.strokeStyle = 'black';
    ctx.arc(0, 0, 4, 0, 2 * Math.PI);
    ctx.stroke();
  
    ctx.beginPath();
    ctx.lineWidth = 1
    ctx.strokeStyle = 'red';
    ctx.arc(0, 0, 4, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.restore()
  }