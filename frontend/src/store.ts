import { AnyAction, combineReducers, Store, createStore, ActionFromReducersMapObject, CombinedState, StateFromReducersMapObject, applyMiddleware, compose } from 'redux';
import { UserSettingsAction, UIStateAction } from './ui/types';
import { userSettings, uiState } from './ui/reducer';
import { MinimapAction } from './minimap/types';
import { minimap } from './minimap/reducer';
import { ContourmapAction } from './contourmap/types';
import { contourmap } from './contourmap/reducer';
//const thunk = require('redux-thunk').default
import { CameraAction } from './camera/types';
import { camera } from './camera/reducer';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import {ThunkAction} from 'redux-thunk';
import { entityRegistry } from './world/reducer';
import {WeaponAction } from './world/weapon/types';
import { replicationMiddleware } from './replication/middleware';
import { replicationReducer, sessionReducer } from './replication/reducer';
import { ReplicationAction } from './replication/types';
import { heightmap } from './heightmap/reducer';
import { TargetAction } from './world/target/types';

// https://github.com/reduxjs/redux-thunk/blob/master/test/typescript.ts
const reducerObject = {
  entities: entityRegistry,
  userSettings,
  uiState,
  minimap,
  contourmap,
  camera,
  counter: (state=0, action: any) => (action.type === "COUNTER_INCREMENT" ? state + 1 : state),
  replication: replicationReducer,
  session: sessionReducer,
  heightmap,
}
//type StoreAction = ActionFromReducersMapObject<typeof reducerObject>
export type StoreAction 
  = UserSettingsAction
  | UIStateAction
  | MinimapAction
  | ContourmapAction
  | CameraAction
  | WeaponAction
  | ReplicationAction
  | TargetAction
  | WeaponAction

export  type ThunkResult<R> = ThunkAction<R, any, undefined, StoreAction>;
//export type StoreState = any
type StoreState = any; //CombinedState<StateFromReducersMapObject<typeof reducerObject>>;
export type Store0 = Store<StoreState, StoreAction>

export const dispatch = (store: Store0, action: StoreAction | ThunkResult<any>) => store.dispatch(action as any);
const reducer = combineReducers(reducerObject);
const w = window as any;

const getMiddlewares = () => {
  let middlewares = []
  middlewares.push(thunk)
  return middlewares;
}


export const newStore = 
  () => createStore(
    reducer,
    compose(applyMiddleware(thunk as ThunkMiddleware<any, StoreAction>, replicationMiddleware))//, w.__REDUX_DEVTOOLS_EXTENSION__ && w.__REDUX_DEVTOOLS_EXTENSION__())
  );
