import {RecordOf, Record } from 'immutable';
import { Action } from 'redux';
import { StoreAction } from '../store';
import { Get } from './types';


export const newRecordReducer = <T, S, K extends keyof S & string>
    (actionType: T, init: () => RecordOf<S>, transition: ((state: RecordOf<S>, payload:  {key: K, value: S[K]}) => RecordOf<S>)) => 
    (state: RecordOf<S>, action: {type: T, payload:  {key: K, value: S[K]}}): RecordOf<S> => 
    {
        if (state === undefined){
            return init();
        }
        if (action.type !== actionType) { 
            return state;
        }
        return transition(state, action.payload);   
    };

export const recordUpdateTransition = <S, K extends keyof S & string>(state: RecordOf<S>, payload: {key: K, value: RecordOf<S>[K]}): RecordOf<S> => {
    const {key, value} = payload;
    if (state.has(key)){
        return state.set(key, value)
    }
    return state;
}


export const basicReducer: <S, A extends Action>(init: () => S, transition: (state: S, action: A) => S) => (state: S, action: A) => S
  // this is mostly a reminder what redux reducers need to do...
  = (init, transition) => (state, action) => {
    if (state === undefined){
      return init();
    }
    //if (actionTypeCheck(action.type)){ 
      return transition(state, action);
    //} else {
    //  return state;
    //}
  }