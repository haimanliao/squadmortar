export type Get<T extends {[key: string]: any}, K extends keyof T> = T[K];

export type WriteAction<T, S, K extends keyof S> = {type: T, payload: {key: K, value: Get<S, K>}}
export type UpdateAction<T, S, K extends keyof S> = {type: T, payload: {key: K, updater: (old: Get<S, K>) => Get<S, K>}}

export type Maybe<T> = T | null