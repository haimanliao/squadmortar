const path = require('path');

module.exports = {
  entry: './src/main.ts',
  mode: "development",
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    filename: "app.js",
    compress: true,
    port: 8000,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'public'),
  },
};