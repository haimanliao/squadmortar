# squadmortar
This is the repository for the website 
[squadmortar.xyz](squadmortar.xyz), a mortar/rocket calculator for the game 
[Squad](https://joinsquad.com/).

### Resources and similar projects

- [Squad Game Mechanics Post](https://www.reddit.com/r/joinsquad/comments/7xeh9g/squad_game_mechanics_post/) and the associated [Game Statistics](https://drive.google.com/drive/folders/1LMXaYgBLMxSoOCWiNNn6oy1TVKAQMbaH) - Data used in this project and every Squad player should know these
- http://squad.ujkl.ru/ - the calculator that earlier versions were based on
- https://squadmc.app - a mobile-friendly alternative

## squadmortar.xyz

### Major features

- One weapon, unlimited number of targets: All info is displayed on the target element itself, making it easier to prepare and switch between a large number of targets.
- Uses heightmaps exported from Squad SDK
- Control via mouse and modifiers: 
  - double-click to create target
  - ctrl-click to delete targets
  - shift-click to set new mortar location
  - drag to move either.
- User settings saved in/loaded from local browser storage
- Weapon selection
    - standard mortar: using (US) milliradians, everything as expected
    - mortar technical: same general properties as the standard mortar, but using degrees instead of milrads
    - UB32 rocket pod technical: A precalculated firing table (distance x height -> angle) allows using this weapon effectively even with significant height differences, which is common on maps where it appears.
- Weapon options
  - Weapon placement helper: Show grid index/keypads while dragging weapon
  - Extra weapon height: Add height over landscape to get accurate data when firing from tall buildings or bridges. Negative values translate to added target height i.e. to fire on top of roofs.
- Target options
    - configurable firing solution text: font size, distance and compact mode (only showing 3 last millirad digits)
    - Aim grid (#): The green grid shows 1° bearing and 5 mil elevation lines, giving you an idea how much you have to change aim in order to get good spacing on rounds.
    - Spread circle (o): The single blue circle shows an approximation of the spread due to inaccuracy (50 MOA). It likely follows normal distribution, though this is unconfirmed.
    - Splash radii ((o)): The red circles show 100 and 25 damage radii, which are instant kills and guaranteed bleed, respectively. If spread is turned on, it shows these from the edge of spread, giving you the worst case damage spread.
    - Compact mode: Shows only last three digits for elevation and no direction.
    - Target placement helper: Show grid index/keypads while dragging target
    - Font size: Adjustable size of target text

### Limitations
- Most extra target features assume the ground around the target is flat and some are (close) geometric simplifications.
- Several properties of the rocket pod calculations add up to make the target area potentially quite inaccurate if you don't know what you're doing.
- Touch screen inputs are not supported and not very high priority.
- Large asset sizes are potentially prohibitive for slow/traffic limited connections.

## Development
    npm install
    npm run dev
    npm run server

Planned:
- Various minor ui improvements
- Collaboration: peer2peer-based multi-user sessions
- Multiple weapons

Nice to have:
- Contour++ maps (see issue)
- Flag layers
- Points of interest
- Improved accuaracy of rocket pod calculations
- Better compression for image assets (e.g. AVIF) / better first-load performance

The majority of the work for the nice-to-have features is doable without having to interact much with existing code so would be easy to contribute, but features need to be maintainable, e.g. "contour maps" would have to be scripts that generate the images from SDK assets not just a folder full of images with no support for updating them.

Note: The features of the mortar calculator aim to improve the performance of an experienced user while also making key mechanics understandable for new users to draw their own conclusions on how best to use mortars. New features should in particular not represent a clear downgrade for an experienced user. It's also meant to be moderately simple to develop/maintain/host. For example, it should work locally-hosted on end-user hardware/network and not be locked into major (infrastructure) frameworks.

### Architecture (WIP)

#### Outer Layer
- HTMLCanvas
- glmatrix
- manually managed global variables

#### Inner Layer
- react: multiple instances for UI overlays
- react-redux
- redux
  - thunks
  - peer.js middleware for message replication

Redux is the core of this latest rewrite and was chosen as a state machine for use with p2p features with the significant benefit of integrating well with react. The entry point is still a classic HTML/CSS/JS website and an SPA by mere coincidence. The canvas and related assets are left out of react to maintain fine-grained control over mutation and ensure optimal performance. 

